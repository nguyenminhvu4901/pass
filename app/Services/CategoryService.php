<?php

namespace App\Services;

use App\Models\Product;
use App\Models\SuperCategory;
use App\Repositories\CategoryRepository;

class CategoryService
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function all()
    {
        return $this->categoryRepository->paginate();
    }

    public function paginate($page)
    {
        return $this->repository->paginate($page);
    }

    public function create($request)
    {
        $data = $request->all();
        return $this->repository->create($data);
    }

    public function findOrFail($id)
    {
        return $this->repository->findOrFail($id);
    }

    public function update($request, $id)
    {
        $data = $request->all();
        return $this->repository->update($data, $id);
    }

    public function delete($id)
    {
        return $this->repository->delete($id);
    }

    public function trash($page = 5)
    {
        return $this->repository->trash($page);
    }

    public function restore($id)
    {
        return $this->repository->restore($id);
    }

    public function forceDelete($id)
    {
        return $this->repository->forceDelete($id);
    }
}