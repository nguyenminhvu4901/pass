<?php
namespace App\Repositories;

abstract class BaseRepository
{
    public $model;

    public function __construct()
    {
        $this->makeModel();
    }

    public function makeModel(): void
    {
        $this->model = app()->make($this->model());
    }

    abstract public function model();

    public function all()
    {
        return $this->model->all();
    }

    public function paginate($limit = null, $columns = ['*'])
    {
        $limit = is_null($limit) ? config('repository.pagination.limit', 10) : $limit;

        return $this->model->paginate($limit, $columns);
    }


    public function find($id, $columns = ['*'])
    {
        return $this->model->findOrFail($id, $columns);
    }

    public function findOrFail($id, $columns = ['*'])
    {
        return $this->model->findOrFail($id);
    }

    public function create(array $input)
    {
        return $this->model->create($input);
    }

    public function update(array $input, $id)
    {
        $model = $this->model->findOrFail($id);
        $model->fill($input);
        $model->save();

        return $model;
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function trash($page)
    {
        return $this->model->onlyTrashed()->paginate($page);
    }

    public function restore($id)
    {
        return $this->model->onlyTrashed()->findOrFail($id)->restore();
    }

    public function forceDelete($id)
    {
        return $this->model->onlyTrashed()->findOrFail($id)->forceDelete();
    }
}