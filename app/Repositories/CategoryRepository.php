<?php
namespace App\Repositories;

use App\Models\Category;
use App\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository
{
    public function model()
    {
        return Category::class;
    }

    public function all()
    {   
        return $this->model->all();
    }

    public function paginate($limit = null, $columns = ['*'])
    {
        return $this->model->with('parent')->paginate($limit, $columns);
    }

    public function findOrFail($id, $columns = ['*'])
    {
        return $this->model->with('children', 'parent')->findOrFail($id, $columns);
    }

    public function create(array $input)
    {
        return $this->model->create($input);
    }

    public function update(array $input, $id)
    {
        $model = $this->model->findOrFail($id);
        $model->fill($input);
        $model->save();

        return $model;

    }

    public function delete($id)
    {
        $model = $this->model->findOrFail($id);
        
        return $model->delete();
    }

    public function trash($page = 5)
    {
        return $this->model->onlyTrashed()->paginate($page);
    }

    public function restore($id)
    {
        return $this->model->onlyTrashed()->findOrFail($id)->restore();
    }

    public function forceDelete($id)
    {
        return $this->model->onlyTrashed()->findOrFail($id)->forceDelete();
    }
}