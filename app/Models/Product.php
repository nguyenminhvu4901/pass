<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Product extends Model
{
    use HasFactory;
    const PATH_IMAGE = 'assets/images/clients/products/';

    const DEFAULT_IMAGE = 'assets/images/clients/custom/default.jpg';

    protected $table = 'products';

    protected $fillable = ['name', 'price', 'description', 'expiry', 'image'];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_product', 'product_id', 'category_id', 'id', 'id');
    }

    public function imagePath() :Attribute
    {
        return Attribute::make(
            get: fn() => !empty($this->image) ? asset(self::PATH_IMAGE . $this->image) : asset(self::DEFAULT_IMAGE)
        );
    }

    public function scopeWithName($query, $name)
    {
        return $query->where('name', 'like', '%'.$name.'%');
    }

    public function scopeWithCategory($query, $category)
    {
        return $query->whereHas('categories', fn ($innerQuery) => $innerQuery->where('name', 'like', '%' . $category . '%'));
    }
    

}
