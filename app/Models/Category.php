<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';

    protected $fillable = ['name', 'parent_id'];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'category_product');
    } 

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function scopeWithName($query, $name)
    {
        return $query->where('name', 'like', '%'.$name.'%');
    }

    public function scopeWithParent($query, $id)
    {
        return $query->whereHas('parent', function ($innerQuery) use ($id) {
            $innerQuery->where('id', $id);
        });  
    }
}
